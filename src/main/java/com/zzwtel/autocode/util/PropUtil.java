package com.zzwtel.autocode.util;

import java.io.InputStream;
import java.util.Properties;



public class PropUtil {
	

	/**
	 * 获取字符串键值
	 * @param k
	 * @return
	 */
	public static String getValue(String k){
		try{
			InputStream is = PropUtil.class.getResourceAsStream("/conf/template.properties");  
	        Properties p = new Properties();  
	        p.load(is);  
	        is.close(); 	       
	        return p.getProperty(k);
		}catch(Exception e){
			e.printStackTrace();
		}		 
		return null;
	}
	
	/**
	 * 获取整型键值
	 * @param k
	 * @param def
	 * @return
	 */
	public static int getInt(String k,int def){
		try{
			String str = getValue(k);
			if(str == null || "".equals(str.trim())){
				return def;
			}
			int v = Integer.parseInt(str);
			return v;
		}catch(Exception e){
			e.printStackTrace();
		}	
		return def;
	}
	
	/**
	 * 获取长整型键值
	 * @param k
	 * @param def
	 * @return
	 */
	public static long getLong(String k,long def){
		try{
			String str = getValue(k);
			if(str == null || "".equals(str.trim())){
				return def;
			}
			long v = Long.parseLong(str);
			return v;
		}catch(Exception e){
			e.printStackTrace();
		}	
		return def;
	}
	
	/**
	 * 获取布尔型键值
	 * @param k
	 * @param def
	 * @return
	 */
	public static boolean getBoolean(String k,boolean def){
		try{
			String str = getValue(k);
			if(str == null || "".equals(str.trim())){
				return def;
			}
			boolean v = Boolean.parseBoolean(str);
			return v;
		}catch(Exception e){
			e.printStackTrace();
		}	
		return def;
	}
	
	public static void main(String[] args){
		
		
		System.out.println(getBoolean("dev",true));
		
		
		System.out.println(getBoolean("dev",false));
	}
}
