package com.zzwtel.autocode.util;

public class PathUtil {
	/**
	 * 获取模版根路径
	 */
	public static String getTemplateRoot(){
		String dir = System.getProperty("user.dir");
		String root = dir + PropUtil.getValue("template.path.beetl");
		return root;
	}
	
	/**
	 * 获取模型根路径
	 */
	public static String getModelRoot(){
		String dir = System.getProperty("user.dir");
		String root = dir + PropUtil.getValue("template.path.model");
		return root;
	}
	/**
	 * 获取输出文件根路径
	 */
	public static String getOutRoot(){
		String dir = System.getProperty("user.dir");
		String root = dir + PropUtil.getValue("template.output.path");
		return root;
	}
	
	/**
	 * 获取Controller模板路径,该路径是相对于根路径的
	 * @return
	 */
	public static String getControllerTemplatePath(){
		return "/java/@{EntityModel}Controller.java";
	} 
	
	
}
