package com.zzwtel.autocode.beetl;

import org.beetl.core.Configuration;
import org.beetl.core.GroupTemplate;
import org.beetl.core.Template;
import org.beetl.core.resource.FileResourceLoader;

import com.zzwtel.autocode.template.constants.TemplatePath;
import com.zzwtel.autocode.template.model.Table;
import com.zzwtel.autocode.template.model.UIModel;
import com.zzwtel.autocode.util.FileUtil;
import com.zzwtel.autocode.util.HumpUtil;
import com.zzwtel.autocode.util.PathUtil;
import com.zzwtel.autocode.util.StrKit;

/**
 * 编辑表单js模板类
 * @author yangtonggan
 * @date 2016-3-8
 */
public class EditFormJsTemplate {
	public void generate(UIModel m,String dir){
		try{				
			String root = PathUtil.getTemplateRoot();
			FileResourceLoader resourceLoader = new FileResourceLoader(root,"utf-8");
			Configuration cfg = Configuration.defaultConfiguration();
			GroupTemplate gt = new GroupTemplate(resourceLoader, cfg);
			//Template t = gt.getTemplate("/js/@{entity_model}_edit_form.js");
			Template t = gt.getTemplate(TemplatePath.EDIT_FORM_JS_TEMPLATE);
			String model = m.getTable().getModel();			
			//首字母小写，驼峰命名
			t.binding("entityModel", model);
			//首字母大写，驼峰命名
			t.binding("EntityModel", StrKit.firstCharToUpperCase(model));
			//全部大写，下划线分割
			String _model = HumpUtil.underscoreName(model);
			t.binding("ENTITY_MODEL", _model);
			//全部小写，下划线分割			
			t.binding("entity_model", HumpUtil.toLowerCase(_model));			
			
			
			String data = t.render();			
			String out = PathUtil.getOutRoot();			
			String fileName = StringTemplate.getFileName(TemplatePath.EDIT_FORM_JS_TEMPLATE,"entity_model",HumpUtil.toLowerCase(_model));		
			FileUtil.write(out+dir+fileName, data);
		}catch(Exception e){
			throw new RuntimeException(e);
		}
	}
	
	
	public static void main(String[] args){
		EditFormJsTemplate eft = new EditFormJsTemplate();
		UIModel m = new UIModel();
		m.setTable(new Table());
		m.getTable().setModel("area");
		eft.generate(m, "system");
	}
}
