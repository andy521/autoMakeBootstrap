package com.zzwtel.autocode.template.model;

import org.jsoup.nodes.Element;
import us.codecraft.xsoup.Xsoup;

/**
 * 二级菜单
 * @author yangtongan
 *<a href="#" id="menu-community-page">小区基本信息</a>
 */
public class SecondLevelMenuModel extends MenuModel{
	//菜单ID
	private String id;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@Override
	public String toXml() {
		//<a href="#" id="menu-community-page">小区基本信息</a>
		StringBuilder sb = new StringBuilder();
		sb.append("    <a href=\"#\" id=\""+getId()+"\">"+getName()+"</a>\n");
		return sb.toString();
	}

	

	@Override
	public MenuModel fromElement(Element ele,int index) throws RuntimeException {
		
		String id  = Xsoup.select(ele, "second_level_menu/@id").get();
		String name  = Xsoup.select(ele, "second_level_menu/@name").get();
		String model = Xsoup.select(ele, "second_level_menu/@model").get();
		setId(id);
		setModel(model);
		setName(name);
		setLevel(2);
		setIndex(index);;
		return this;
	}
	
	
}
