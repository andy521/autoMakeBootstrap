package com.zzwtel.autocode.template.model;

import com.zzwtel.autocode.util.HumpUtil;

public class UrlModel {
	
	public String getENTITY_MODEL() {
		return ENTITY_MODEL;
	}
	public void setENTITY_MODEL(String eNTITY_MODEL) {
		ENTITY_MODEL = eNTITY_MODEL;
	}
	public String getEntity_model() {
		return entity_model;
	}
	public void setEntity_model(String entity_model) {
		this.entity_model = entity_model;
	}
	
	private String ENTITY_MODEL;
	private String entity_model;
	
	public UrlModel(Table t){
		String model = t.getModel();	
		//全部大写，下划线分割
		String _model = HumpUtil.underscoreName(model);
		this.ENTITY_MODEL = _model;
		this.entity_model = HumpUtil.toLowerCase(_model);
	}
	
}
