package com.zzwtec.community.action.system;


import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.BeanUtils;

import com.jfinal.core.ActionKey;
import com.jfinal.core.Controller;
import com.zzwtec.community.common.DataObject;
import com.zzwtec.community.common.IceServiceUtil;
import com.zzwtec.community.common.Page;
import com.zzwtec.community.common.UrlConstants;
import com.zzwtec.community.model.Area;
import com.zzwtec.community.model.BuildM;
import com.zzwtec.community.model.Cell;
import com.zzwtec.community.model.CellM;
import com.zzwtec.community.model.CellMPage;
import com.zzwtec.community.model.CommunityM;
import com.zzwtec.community.model.CommunityMPage;
import com.zzwtec.community.service.BuildServicePrx;
import com.zzwtec.community.service.CellServicePrx;
import com.zzwtec.community.service.CommunityServicePrx;

/**
 * Cell控制器
 * @author yangtonggan
 * @date  2016-04-01
 * 
 */
public class CellController extends Controller {
	/**
	 * 获取区域列表
	 */
	@ActionKey(UrlConstants.URL_CELL)
	public void list(){				
		//CellServicePrx prx = IceServiceUtil.getService(CellServicePrx.class);
		//query(prx);
		// 构造测试数据	
		buildTestData();
		render("/system/view/cell_list.html");
	}
	
	/**
	 * 添加Cell
	 */
	@ActionKey(UrlConstants.URL_CELL_ADD)
	public void add(){		
		DataObject repJson = new DataObject("添加Cell成功");;
		try{
			//CellServicePrx prx = IceServiceUtil.getService(CellServicePrx.class);
			Cell model = getBean(Cell.class);		
			//prx.addCell(model);				
		}catch(Exception e){
			repJson = new DataObject("添加Cell失败");
		}finally{
			renderJson(repJson);
		}	
		
	}
	
	/**
	 * 删除Cell
	 */
	@ActionKey(UrlConstants.URL_CELL_DEL)
	public void del(){		
		DataObject repJson = new DataObject("删除Cell成功");
		try{
			String ids = getPara("ids");		
			//CellServicePrx prx = IceServiceUtil.getService(CellServicePrx.class);
			//prx.delCellByIds(ids.split(","));
		}catch(Exception e){
			repJson = new DataObject("删除Cell失败");
		}finally{
			renderJson(repJson);
		}
	}
	
	/**
	 * 获取添加表单界面
	 */
	@ActionKey(UrlConstants.URL_CELL_ADD_FORM_UI)
	public void addFormUI(){
		//TODO此处可以做一些预处理，比如为下拉列表赋值
		Cell entity = new Cell();	
		setAttr("cell", entity);
		render("/system/view/cell_add_form.html");
	}
	
	/**
	 * 根据ID获取一条记录,并且返回编辑界面UI
	 */
	@ActionKey(UrlConstants.URL_CELL_EDIT_FORM_UI)
	public void editFormUI(){	
		try{			
			String id = getPara("id");	
			//CellServicePrx prx = IceServiceUtil.getService(CellServicePrx.class);
			//CellM modele = prx.inspectCell(id);
			Cell entity = new Cell();		
			//BeanUtils.copyProperties(modele, entity);	
			//TODO需要对界面下拉列表做预处理
			setAttr("cell", entity);			
			render("/system/view/cell_edit_form.html");
		}catch(Exception e){
			DataObject repJson = new DataObject("获取Cell失败");
			renderJson(repJson);
		}
	}	
	
	
	/**
	 * 修改Cell
	 */
	@ActionKey(UrlConstants.URL_CELL_UPD)
	public void upd(){
		DataObject repJson = new DataObject("更新Cell成功");
		try{
			//CellServicePrx prx = IceServiceUtil.getService(CellServicePrx.class);
			Cell model = getBean(Cell.class);
			//prx.alterCell(model);
		}catch(Exception e){
			repJson = new DataObject("更新Cell失败");
		}finally{
			renderJson(repJson);
		}		
	}
	
	private void query(CellServicePrx prx){		
		Page page = new Page();		
		Map<String, String> term = new HashMap<String, String>();
		/*
		String name = getPara("name");
		term.put("name", name);
		//请参考注释代码设置查询条件
		*/
		//CellMPage viewModel = prx.getCellList(page, term);			
		//setAttr("viewData", viewModel);		
	}	
	
	private void buildTestData(){		
		Cell[] aray = new Cell[12];		
		for(int i=0;i<12;i++){
			aray[i] = new Cell();
			aray[i].id = "id-"+i;			
		}	
		List<Cell> pageList = Arrays.asList(aray);		
		setAttr("pageList", pageList);		
	}
	
}
