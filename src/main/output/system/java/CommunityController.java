package com.zzwtec.community.action.system;


import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.BeanUtils;

import com.jfinal.core.ActionKey;
import com.jfinal.core.Controller;
import com.zzwtec.community.common.DataObject;
import com.zzwtec.community.common.IceServiceUtil;
import com.zzwtec.community.common.Page;
import com.zzwtec.community.common.UrlConstants;
import com.zzwtec.community.model.Area;
import com.zzwtec.community.model.BuildM;
import com.zzwtec.community.model.Community;
import com.zzwtec.community.model.CommunityM;
import com.zzwtec.community.model.CommunityMPage;
import com.zzwtec.community.model.CommunityM;
import com.zzwtec.community.model.CommunityMPage;
import com.zzwtec.community.service.BuildServicePrx;
import com.zzwtec.community.service.CommunityServicePrx;
import com.zzwtec.community.service.CommunityServicePrx;

/**
 * Community控制器
 * @author yangtonggan
 * @date  2016-04-01
 * 
 */
public class CommunityController extends Controller {
	/**
	 * 获取区域列表
	 */
	@ActionKey(UrlConstants.URL_COMMUNITY)
	public void list(){				
		//CommunityServicePrx prx = IceServiceUtil.getService(CommunityServicePrx.class);
		//query(prx);
		// 构造测试数据	
		buildTestData();
		render("/system/view/community_list.html");
	}
	
	/**
	 * 添加Community
	 */
	@ActionKey(UrlConstants.URL_COMMUNITY_ADD)
	public void add(){		
		DataObject repJson = new DataObject("添加Community成功");;
		try{
			//CommunityServicePrx prx = IceServiceUtil.getService(CommunityServicePrx.class);
			Community model = getBean(Community.class);		
			//prx.addCommunity(model);				
		}catch(Exception e){
			repJson = new DataObject("添加Community失败");
		}finally{
			renderJson(repJson);
		}	
		
	}
	
	/**
	 * 删除Community
	 */
	@ActionKey(UrlConstants.URL_COMMUNITY_DEL)
	public void del(){		
		DataObject repJson = new DataObject("删除Community成功");
		try{
			String ids = getPara("ids");		
			//CommunityServicePrx prx = IceServiceUtil.getService(CommunityServicePrx.class);
			//prx.delCommunityByIds(ids.split(","));
		}catch(Exception e){
			repJson = new DataObject("删除Community失败");
		}finally{
			renderJson(repJson);
		}
	}
	
	/**
	 * 获取添加表单界面
	 */
	@ActionKey(UrlConstants.URL_COMMUNITY_ADD_FORM_UI)
	public void addFormUI(){
		//TODO此处可以做一些预处理，比如为下拉列表赋值
		Community entity = new Community();	
		setAttr("community", entity);
		render("/system/view/community_add_form.html");
	}
	
	/**
	 * 根据ID获取一条记录,并且返回编辑界面UI
	 */
	@ActionKey(UrlConstants.URL_COMMUNITY_EDIT_FORM_UI)
	public void editFormUI(){	
		try{			
			String id = getPara("id");	
			//CommunityServicePrx prx = IceServiceUtil.getService(CommunityServicePrx.class);
			//CommunityM modele = prx.inspectCommunity(id);
			Community entity = new Community();		
			//BeanUtils.copyProperties(modele, entity);	
			//TODO需要对界面下拉列表做预处理
			setAttr("community", entity);			
			render("/system/view/community_edit_form.html");
		}catch(Exception e){
			DataObject repJson = new DataObject("获取Community失败");
			renderJson(repJson);
		}
	}	
	
	
	/**
	 * 修改Community
	 */
	@ActionKey(UrlConstants.URL_COMMUNITY_UPD)
	public void upd(){
		DataObject repJson = new DataObject("更新Community成功");
		try{
			//CommunityServicePrx prx = IceServiceUtil.getService(CommunityServicePrx.class);
			Community model = getBean(Community.class);
			//prx.alterCommunity(model);
		}catch(Exception e){
			repJson = new DataObject("更新Community失败");
		}finally{
			renderJson(repJson);
		}		
	}
	
	private void query(CommunityServicePrx prx){		
		Page page = new Page();		
		Map<String, String> term = new HashMap<String, String>();
		/*
		String name = getPara("name");
		term.put("name", name);
		//请参考注释代码设置查询条件
		*/
		//CommunityMPage viewModel = prx.getCommunityList(page, term);			
		//setAttr("viewData", viewModel);		
	}	
	
	private void buildTestData(){		
		Community[] aray = new Community[12];		
		for(int i=0;i<12;i++){
			aray[i] = new Community();
			aray[i].id = "id-"+i;			
		}	
		List<Community> pageList = Arrays.asList(aray);		
		setAttr("pageList", pageList);		
	}
	
}
