package com.zzwtec.community.action.system;


import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.BeanUtils;

import com.jfinal.core.ActionKey;
import com.jfinal.core.Controller;
import com.zzwtec.community.common.DataObject;
import com.zzwtec.community.common.IceServiceUtil;
import com.zzwtec.community.common.Page;
import com.zzwtec.community.common.UrlConstants;
import com.zzwtec.community.model.Area;
import com.zzwtec.community.model.BuildM;
import com.zzwtec.community.model.Ad;
import com.zzwtec.community.model.AdM;
import com.zzwtec.community.model.AdMPage;
import com.zzwtec.community.model.CommunityM;
import com.zzwtec.community.model.CommunityMPage;
import com.zzwtec.community.service.BuildServicePrx;
import com.zzwtec.community.service.AdServicePrx;
import com.zzwtec.community.service.CommunityServicePrx;

/**
 * Ad控制器
 * @author yangtonggan
 * @date  2016-04-01
 * 
 */
public class AdController extends Controller {
	/**
	 * 获取区域列表
	 */
	@ActionKey(UrlConstants.URL_AD)
	public void list(){				
		//AdServicePrx prx = IceServiceUtil.getService(AdServicePrx.class);
		//query(prx);
		// 构造测试数据	
		buildTestData();
		render("/system/view/ad_list.html");
	}
	
	/**
	 * 添加Ad
	 */
	@ActionKey(UrlConstants.URL_AD_ADD)
	public void add(){		
		DataObject repJson = new DataObject("添加Ad成功");;
		try{
			//AdServicePrx prx = IceServiceUtil.getService(AdServicePrx.class);
			Ad model = getBean(Ad.class);		
			//prx.addAd(model);				
		}catch(Exception e){
			repJson = new DataObject("添加Ad失败");
		}finally{
			renderJson(repJson);
		}	
		
	}
	
	/**
	 * 删除Ad
	 */
	@ActionKey(UrlConstants.URL_AD_DEL)
	public void del(){		
		DataObject repJson = new DataObject("删除Ad成功");
		try{
			String ids = getPara("ids");		
			//AdServicePrx prx = IceServiceUtil.getService(AdServicePrx.class);
			//prx.delAdByIds(ids.split(","));
		}catch(Exception e){
			repJson = new DataObject("删除Ad失败");
		}finally{
			renderJson(repJson);
		}
	}
	
	/**
	 * 获取添加表单界面
	 */
	@ActionKey(UrlConstants.URL_AD_ADD_FORM_UI)
	public void addFormUI(){
		//TODO此处可以做一些预处理，比如为下拉列表赋值
		Ad entity = new Ad();	
		setAttr("ad", entity);
		render("/system/view/ad_add_form.html");
	}
	
	/**
	 * 根据ID获取一条记录,并且返回编辑界面UI
	 */
	@ActionKey(UrlConstants.URL_AD_EDIT_FORM_UI)
	public void editFormUI(){	
		try{			
			String id = getPara("id");	
			//AdServicePrx prx = IceServiceUtil.getService(AdServicePrx.class);
			//AdM modele = prx.inspectAd(id);
			Ad entity = new Ad();		
			//BeanUtils.copyProperties(modele, entity);	
			//TODO需要对界面下拉列表做预处理
			setAttr("ad", entity);			
			render("/system/view/ad_edit_form.html");
		}catch(Exception e){
			DataObject repJson = new DataObject("获取Ad失败");
			renderJson(repJson);
		}
	}	
	
	
	/**
	 * 修改Ad
	 */
	@ActionKey(UrlConstants.URL_AD_UPD)
	public void upd(){
		DataObject repJson = new DataObject("更新Ad成功");
		try{
			//AdServicePrx prx = IceServiceUtil.getService(AdServicePrx.class);
			Ad model = getBean(Ad.class);
			//prx.alterAd(model);
		}catch(Exception e){
			repJson = new DataObject("更新Ad失败");
		}finally{
			renderJson(repJson);
		}		
	}
	
	private void query(AdServicePrx prx){		
		Page page = new Page();		
		Map<String, String> term = new HashMap<String, String>();
		/*
		String name = getPara("name");
		term.put("name", name);
		//请参考注释代码设置查询条件
		*/
		//AdMPage viewModel = prx.getAdList(page, term);			
		//setAttr("viewData", viewModel);		
	}	
	
	private void buildTestData(){		
		Ad[] aray = new Ad[12];		
		for(int i=0;i<12;i++){
			aray[i] = new Ad();
			aray[i].id = "id-"+i;			
		}	
		List<Ad> pageList = Arrays.asList(aray);		
		setAttr("pageList", pageList);		
	}
	
}
