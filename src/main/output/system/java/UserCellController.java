package com.zzwtec.community.action.system;


import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.BeanUtils;

import com.jfinal.core.ActionKey;
import com.jfinal.core.Controller;
import com.zzwtec.community.common.DataObject;
import com.zzwtec.community.common.IceServiceUtil;
import com.zzwtec.community.common.Page;
import com.zzwtec.community.common.UrlConstants;
import com.zzwtec.community.model.Area;
import com.zzwtec.community.model.BuildM;
import com.zzwtec.community.model.UserCell;
import com.zzwtec.community.model.UserCellM;
import com.zzwtec.community.model.UserCellMPage;
import com.zzwtec.community.model.CommunityM;
import com.zzwtec.community.model.CommunityMPage;
import com.zzwtec.community.service.BuildServicePrx;
import com.zzwtec.community.service.UserCellServicePrx;
import com.zzwtec.community.service.CommunityServicePrx;

/**
 * UserCell控制器
 * @author yangtonggan
 * @date  2016-04-01
 * 
 */
public class UserCellController extends Controller {
	/**
	 * 获取区域列表
	 */
	@ActionKey(UrlConstants.URL_USER_CELL)
	public void list(){				
		//UserCellServicePrx prx = IceServiceUtil.getService(UserCellServicePrx.class);
		//query(prx);
		// 构造测试数据	
		buildTestData();
		render("/system/view/user_cell_list.html");
	}
	
	/**
	 * 添加UserCell
	 */
	@ActionKey(UrlConstants.URL_USER_CELL_ADD)
	public void add(){		
		DataObject repJson = new DataObject("添加UserCell成功");;
		try{
			//UserCellServicePrx prx = IceServiceUtil.getService(UserCellServicePrx.class);
			UserCell model = getBean(UserCell.class);		
			//prx.addUserCell(model);				
		}catch(Exception e){
			repJson = new DataObject("添加UserCell失败");
		}finally{
			renderJson(repJson);
		}	
		
	}
	
	/**
	 * 删除UserCell
	 */
	@ActionKey(UrlConstants.URL_USER_CELL_DEL)
	public void del(){		
		DataObject repJson = new DataObject("删除UserCell成功");
		try{
			String ids = getPara("ids");		
			//UserCellServicePrx prx = IceServiceUtil.getService(UserCellServicePrx.class);
			//prx.delUserCellByIds(ids.split(","));
		}catch(Exception e){
			repJson = new DataObject("删除UserCell失败");
		}finally{
			renderJson(repJson);
		}
	}
	
	/**
	 * 获取添加表单界面
	 */
	@ActionKey(UrlConstants.URL_USER_CELL_ADD_FORM_UI)
	public void addFormUI(){
		//TODO此处可以做一些预处理，比如为下拉列表赋值
		UserCell entity = new UserCell();	
		setAttr("userCell", entity);
		render("/system/view/user_cell_add_form.html");
	}
	
	/**
	 * 根据ID获取一条记录,并且返回编辑界面UI
	 */
	@ActionKey(UrlConstants.URL_USER_CELL_EDIT_FORM_UI)
	public void editFormUI(){	
		try{			
			String id = getPara("id");	
			//UserCellServicePrx prx = IceServiceUtil.getService(UserCellServicePrx.class);
			//UserCellM modele = prx.inspectUserCell(id);
			UserCell entity = new UserCell();		
			//BeanUtils.copyProperties(modele, entity);	
			//TODO需要对界面下拉列表做预处理
			setAttr("userCell", entity);			
			render("/system/view/user_cell_edit_form.html");
		}catch(Exception e){
			DataObject repJson = new DataObject("获取UserCell失败");
			renderJson(repJson);
		}
	}	
	
	
	/**
	 * 修改UserCell
	 */
	@ActionKey(UrlConstants.URL_USER_CELL_UPD)
	public void upd(){
		DataObject repJson = new DataObject("更新UserCell成功");
		try{
			//UserCellServicePrx prx = IceServiceUtil.getService(UserCellServicePrx.class);
			UserCell model = getBean(UserCell.class);
			//prx.alterUserCell(model);
		}catch(Exception e){
			repJson = new DataObject("更新UserCell失败");
		}finally{
			renderJson(repJson);
		}		
	}
	
	private void query(UserCellServicePrx prx){		
		Page page = new Page();		
		Map<String, String> term = new HashMap<String, String>();
		/*
		String name = getPara("name");
		term.put("name", name);
		//请参考注释代码设置查询条件
		*/
		//UserCellMPage viewModel = prx.getUserCellList(page, term);			
		//setAttr("viewData", viewModel);		
	}	
	
	private void buildTestData(){		
		UserCell[] aray = new UserCell[12];		
		for(int i=0;i<12;i++){
			aray[i] = new UserCell();
			aray[i].id = "id-"+i;			
		}	
		List<UserCell> pageList = Arrays.asList(aray);		
		setAttr("pageList", pageList);		
	}
	
}
